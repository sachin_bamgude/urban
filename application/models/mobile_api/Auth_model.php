<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public $table1 = 'users';    
  
    public function register($post_data)
    {  
        //print_r($post_data);exit();
        $lastdate = date("Y-m-d h:i:s");
        $post_data['lastLogin'] = $lastdate;
        $password = $post_data['password'];
        if(! empty($password)){
           $post_data['password'] = do_hash($password); 
        }        

        $profile_picture = array_key_exists("profile_picture",$post_data);
        if($profile_picture){
            unset($post_data['profile_picture']);
            $post_data['profile_picture'] = $_POST['profile_picture1'];   
        }  

        //print_r($post_data);exit();

        $is_register = $this->db->insert($this->table1, $post_data);
        if($is_register){
            $userId = $this->db->insert_id();
            $email = $post_data['email'];
            /*Update the touserId if user exist in Tribe table */
            $sql = "SELECT * FROM tribe WHERE inviteEmail='".$email."'";
            $record = $this->db->query($sql);
            if($record->num_rows()>0){
                /*Update the userId in tribe table*/
                $touserId = array('touserId'=>$userId);
                $this->db->where('inviteEmail',$email);
                return $this->db->update('tribe',$touserId);
            }else{
                return true;
            }            
        }

    }    
        
    public function get_facebook_user($facebookId)
    {
        $sql1 = "SELECT userId,firstName,lastName,email,profile_picture,age,gender,workEducation,city,'password' loggedin_with,connectSherpaFlag,latitude,longitude,deviceToken,facebookId,deleteFlag,lastLogin,createdAt FROM users WHERE facebookId = '".$facebookId."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {                
            return $record1->result_array();
        }        
    }
    
     public function email_check($email)
    {
        $sql1 = "SELECT userId FROM users WHERE email='".$email."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {
            return true;
        }
    }

    public function login($post_data)
    {    
        $lastdate = date("Y-m-d h:i:s");
        $email = $post_data['email'];
        $password = $post_data['password'];
        $password = do_hash($password);
        $device_token = $post_data['deviceToken'];
        $sql1 = "SELECT * FROM users WHERE email = '".$email."' AND password = '".$password."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {
            $lastdate_update = "UPDATE users SET lastLogin ='".$lastdate."' WHERE email ='".$email."'";
                $result = $this->db->query($lastdate_update);
            return $record1->result_array();            
        }
        else{
            return false;
        }
    }

    public function logout($post_data)
    {
        $userId = $post_data['userId'];
        $sql1 = "SELECT userId FROM users WHERE userId = '".$userId."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {
                $device_token_update = "UPDATE users SET deviceToken ='' WHERE userId='".$userId."'";
                $result = $this->db->query($device_token_update);
            return true;
        }

    }

    /*function for make sure the  student id exists*/
    public function user_check($userId)
    {
        $sql1 = "SELECT userId FROM users WHERE userId ='".$userId."'";
        $record = $this->db->query($sql1);
        if ($record->num_rows()>0) {
            return true;
        }   
    }  
  
    /* Save tokens */
    public function save_token_with_expiry($post_data,$email)
    {
        if (empty($post_data['latitude'])) {
            unset($post_data['latitude']);
        }
        if (empty($post_data['longitude'])) {
            unset($post_data['longitude']);
        }
        unset($post_data['email']);
        unset($post_data['password']);
        unset($post_data['profile_picture']);
        return $this->db->where('email',$email)->update($this->table1, $post_data);
    }

    public function refresh_token_check($refresh_token)
    {
        $sql = "SELECT refreshTokenExpiry FROM users WHERE refreshToken='".$refresh_token."'";
        $record = $this->db->query($sql);        
        if ($record->num_rows()>0) {
            return $record->row('refreshTokenExpiry');            
        }
    }

    public function access_token($post_data)
    {
        if (empty($post_data['latitude'])) {
            unset($post_data['latitude']);
        }
        if (empty($post_data['longitude'])) {
            unset($post_data['longitude']);
        }

        return $this->db->where('refreshToken',$post_data['refreshToken'])->update($this->table1, $post_data);
    }
	
	
    /*****************************************************************************************/

    /*function for check email exists or not */
     public function email_exists($email)
    {
        $sql1 = "SELECT userId FROM users WHERE email='".$email."'";
        $record1 = $this->db->query($sql1);        
        if ($record1->num_rows()>0) {
            return true;
        }else
        {
           return false;
        }
        
    }

    /* forgot password */
    public function fogotPassword($post_data)
    {
        $email = $post_data['email'];
        $sql = "SELECT userId,firstName,email FROM users WHERE email='".$email."'";
        $record = $this->db->query($sql);
        if ($record->num_rows()>0) {
                //return $record->row('student_id');
                return $record->result_array();
        }else{
                return false;
            }
    }    
    
    

    
}?>