<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Common_model extends CI_Model
{
    
    /*function for make sure the  student id exists*/
    public function user_check($userId)
    {
        $sql1 = "SELECT userId FROM users WHERE userId ='".$userId."'";
        $record = $this->db->query($sql1);
        if ($record->num_rows()>0) {
            return true;
        }   
    }  

    /*function for make sure the  search criteria exists*/
    public function search_criteria_check($searchId)
    {
        $sql1 = "SELECT searchId FROM searchcriteria WHERE searchId ='".$searchId."'";
        $record = $this->db->query($sql1);
        if ($record->num_rows()>0) {
            return true;
        }   
    }  

    /*function for make sure the  shortlisted property id exists*/
    public function shortlisted_property_check($shortlistedId)
    {
        $sql1 = "SELECT shortlistedId FROM shortlistedproperty WHERE shortlistedId ='".$shortlistedId."'";
        $record = $this->db->query($sql1);
        if ($record->num_rows()>0) {
            return true;
        }   
    }  

    /* Get Profile details */
    public function profile_detail($post_data){
        $userId = $post_data['userId'];

        $sql = "SELECT userId,firstName,email,password,profile_picture,age,gender,workEducation,city,deviceToken,facebookId,token,tokenExpiry,refreshToken,refreshTokenExpiry,latitude,longitude,lastLogin,connectSherpaFlag FROM users WHERE userId = $userId";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }

    }

    /* Update Profile details */
     public function updateProfile($post_data)
    {

        $userId = $post_data['userId'];

        $query = $this->db->where('userId',$userId)->update('users', $post_data);        
        if($query){
            return $post_data;
        }
    }

    /* Check the email for user is exist */
    public function update_email_check($email,$userId)
    {
        $sql1 = "SELECT userId FROM users WHERE userId = $userId AND email='".$email."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()==1) {
            return false;
        } else {
            return true;
        }
    }

     
   
   /////////////////////////////////////////////////////////////////////////////////////////

    /* Save Basic search criteria */
    public function save_search_criteria($post_data){
        unset($post_data['token']);
        // $userId = $post_data['userId'];
        //print_r($post_data);exit();
        $commute = array_key_exists("commute",$post_data);
        if($commute){ 
            $commute =  $post_data['commute']; 
            unset($post_data['commute']);

        }

        $preferences = array_key_exists("preferences",$post_data);
        if($preferences){            
            $preferences = $post_data['preferences']; 
            $post_data['proximity'] = implode(",",$preferences['proximity']);
            $post_data['avoid_proximity'] = implode(",",$preferences['avoid_proximity']);
            unset($post_data['preferences']);
        } 

        $advanceSearch = array_key_exists("advanceSearch",$post_data);
        if($advanceSearch){            
            $advanceSearch = $post_data['advanceSearch'];        
            $post_data['area'] = $advanceSearch['area'];
            $post_data['keywords'] = implode(",",$advanceSearch['keywords']);
            $post_data['avoid_area'] = $advanceSearch['avoid_area'];
            $post_data['avoid_keywords'] = implode(",",$advanceSearch['avoid_keywords']);  
            unset($post_data['advanceSearch']);
        } 

        
        $topSearch = array_key_exists("topSearch",$post_data);
        if($topSearch){
            $topSearch = $post_data['topSearch'];
            $post_data['criteria1'] = $topSearch['criteria1'];
            $post_data['criteria2'] = $topSearch['criteria2'];
            $post_data['criteria3'] = $topSearch['criteria3'];
            unset($post_data['topSearch']);
        }
         //print_r($post_data);exit();
        // check searchId is present
        $searchId = $post_data['searchId'];
        if($searchId){
            $update = $this->db->where('searchId',$searchId)->update('searchcriteria', $post_data);
            if($update){   
                /* Delete the previous commute */
                $this->db->where('userId', $post_data['userId']);
                $this->db->where('searchId', $searchId);
                if($this->db->delete('commute')){
                    /* Insert data in commute table */
                    $commute_details = array();
                    foreach($commute as $key){                
                            $commute_data = array(
                                "userId"=>$post_data['userId'],
                                "searchId" => $searchId,
                                "commuteName"=> $key['commuteName'],
                                "destination"=> $key['destination'],
                                //"timeofCommuting"=> $key['timeofCommuting'],
                                "maxCommuteTime"=> $key['maxCommuteTime'],
                                "destLatitude"=> $key['destLatitude'],
                                "destLongitude"=> $key['destLongitude'],
                                "primaryCommute"=> $key['primaryCommute']
                            );
                            $commute_details[] =  $commute_data;
                        }                        
                  // print_r($commute_details);exit();
                    $this->db->insert_batch('commute', $commute_details);  
                }            
                 
                $_POST['message'] = "Search criteria updated successfully !";
                return $searchId;
            }

        }else{
            // echo "insert";exit();
            $insert =  $this->db->insert("searchcriteria", $post_data);
            if($insert){
                $searchId = $this->db->insert_id();
                /* Insert data in commute table */
               // print_r($post_data['commute']);exit();
                $commute_details = array();
                foreach($commute as $key){                
                        $commute_data = array(
                            "userId"=>$post_data['userId'],
                            "searchId" => $searchId,
                            "commuteName"=> $key['commuteName'],
                            "destination"=> $key['destination'],
                            //"timeofCommuting"=> $key['timeofCommuting'],
                            "maxCommuteTime"=> $key['maxCommuteTime'],
                            "destLatitude"=> $key['destLatitude'],
                            "destLongitude"=> $key['destLongitude'],
                            "primaryCommute"=> $key['primaryCommute']
                        );
                        $commute_details[] =  $commute_data;
                    }
                        
                $this->db->insert_batch('commute', $commute_details); 
                $_POST['message'] = "Search criteria added successfully !";
                return $searchId;
            }
        }
    }

    /* Delete search criteria */
    public function delete_search_criteria($post_data){
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        $data = array('deleteFlag' => 1);

        $this->db->where('userId',$userId);
        $this->db->where('searchId',$searchId);   
        $this->db->update('searchcriteria',$data); 
        if($this->db->affected_rows()>0){
            $this->db->where('userId',$userId);
            $this->db->where('searchId',$searchId); 
            $this->db->update('commute',$data);
           if($this->db->affected_rows()>0)
           {
             return true;
           }else{
             return false;
           }
        }
    }
   
   /* count search criteria list */
   public function count_search_criteria_list($post_data){
        $userId = $post_data['userId'];              
        $date = date("Y-m-d H:i:s");
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR s.idealMovingDate LIKE '%$keyword%' OR s.houseType LIKE '%$keyword%' OR s.priceRange LIKE '%$keyword%' OR s.bedroom LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if (!empty($latitude) && !empty($longitude)) {

            $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) from tribe t1 WHERE t1.searchId = s.searchId AND t1.status ='accept') +1)  as participantCount,
                (SELECT COUNT(*) from shortlistedproperty sp WHERE sp.searchId = s.searchId AND sp.deleteFlag !=1) as properties
                 FROM searchcriteria s  
                    left join tribe t on s.searchId = t.searchId AND t.fromuserId = $userId
                    left join users u on s.userId = u.userId
                    left join shortlistedproperty p on s.searchId = p.searchId 
                    where s.searchId in (SELECT t.searchId from tribe t where (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId $keyword
                    GROUP BY s.searchId ORDER BY s.userId = $userId DESC";
                    
        }else{
             $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) from tribe t1 WHERE t1.searchId = s.searchId AND t1.status ='accept')+1) as participantCount,
                (SELECT COUNT(*) from shortlistedproperty sp WHERE sp.searchId = s.searchId AND sp.deleteFlag !=1) as properties
                 FROM searchcriteria s  
                    left join tribe t on s.searchId = t.searchId AND t.fromuserId = $userId
                    left join users u on s.userId = u.userId
                    left join shortlistedproperty p on s.searchId = p.searchId 
                    where s.searchId in (SELECT t.searchId from tribe t where (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId $keyword
                    GROUP BY s.searchId ORDER BY s.userId = $userId DESC";
                    
        }

        $record = $this->db->query($sql);
        return $record->num_rows();
   }

   /* Search criteria list */
   public function search_criteria_list($post_data){
        $userId = $post_data['userId'];              
        $date = date("Y-m-d H:i:s");
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR s.idealMovingDate LIKE '%$keyword%' OR s.houseType LIKE '%$keyword%' OR s.priceRange LIKE '%$keyword%' OR s.bedroom LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if (!empty($latitude) && !empty($longitude)) {

            /* $sql ="SELECT s.*,u.userId,u.firstName,u.lastName,
                    (SELECT count(*) FROM tribe t WHERE u.userId = t.fromuserId AND t.searchId = s.searchId AND t.fromuserId = $userId  ) as participant,
                    $distance_in_km 
                FROM searchcriteria s,users u WHERE  u.userId = s.userId   AND s.deleteFlag != 1  AND u.userId =$userId  $keyword LIMIT $offset, $limit";
            */
                $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) from tribe t1 WHERE t1.searchId = s.searchId AND t1.status ='accept')+1) as participantCount,
                (SELECT COUNT(*) from shortlistedproperty sp WHERE sp.searchId = s.searchId AND sp.deleteFlag !=1) as properties
                 FROM searchcriteria s  
                    left join tribe t on s.searchId = t.searchId AND t.fromuserId = $userId
                    left join users u on s.userId = u.userId
                    left join shortlistedproperty p on s.searchId = p.searchId 
                    where s.searchId in (SELECT t.searchId from tribe t where (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId $keyword
                    GROUP BY s.searchId ORDER BY s.userId = $userId DESC LIMIT $offset, $limit";
        }else{
            
            /* $sql ="SELECT s.*,u.userId,u.firstName,u.lastName,
                    (SELECT count(*) FROM tribe t WHERE u.userId = t.fromuserId AND t.searchId = s.searchId AND t.fromuserId = $userId  ) as participant
                 FROM searchcriteria s,users u WHERE  u.userId = s.userId   AND s.deleteFlag != 1  AND u.userId =$userId  $keyword LIMIT $offset, $limit";
            */

                $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) from tribe t1 WHERE t1.searchId = s.searchId AND t1.status ='accept')+1) as participantCount,
                (SELECT COUNT(*) from shortlistedproperty sp WHERE sp.searchId = s.searchId AND sp.deleteFlag !=1) as properties
                 FROM searchcriteria s  
                    left join tribe t on s.searchId = t.searchId AND t.fromuserId = $userId
                    left join users u on s.userId = u.userId
                    left join shortlistedproperty p on s.searchId = p.searchId 
                    where s.searchId in (SELECT t.searchId from tribe t where (t.touserId = $userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId  $keyword
                    GROUP BY s.searchId ORDER BY s.userId = $userId DESC LIMIT $offset, $limit";
        }

        //echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }        
   }

   /* function for search criteria details */
    public function get_commute_detail($userId,$searchId){
       // $userId = $post_data['userId'];
       // $searchId = $post_data['searchId'];

        $sql ="SELECT c.commuteId,c.commuteName,c.destination,c.maxCommuteTime,c.destLatitude,c.destLongitude,c.primaryCommute                
                FROM commute c,users u WHERE u.userId = c.userId  AND c.searchId = $searchId AND c.deleteFlag != 1 AND c.userId =$userId";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();            
        } 
   }


    /* function for search criteria details */
    public function get_preference_detail($userId,$searchId){
        //$userId = $post_data['userId'];
        //$searchId = $post_data['searchId'];

        $sql ="SELECT s.*,u.userId,u.firstName,u.lastName
                            
                FROM searchcriteria s,users u WHERE u.userId = s.userId  AND s.deleteFlag != 1  AND s.searchId = $searchId  AND s.userId =$userId";
        
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array()[0];
            //print_r($record->result_array());exit();
        } 
   }

   /* save tribe using email */
   public function save_tribe($post_data){
        //print_r($post_data);exit();
        unset($post_data['token']);
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        //print_r($post_data);exit();
        $invitees = array_key_exists("invitees",$post_data);        
        if($invitees){ 
           // $invitees =  $post_data['invitees']; 
            $invitees =  $post_data['invitees']; 
           //  $emails = array();
           //  for($i=0;$i<sizeof($invitees);$i++){
           //      $emails[$i]['inviteEmail'] = $invitees[$i]['inviteEmail'];
           //  }
           // //print_r($emails);exit();
           //  $str_email = implode(',', array_map(function($el){ return $el['inviteEmail']; }, $emails));
           //  $str = "'".$str_email."'";
           //  $email =  str_replace(",","','",$str);
            
            unset($post_data['invitees']);
        }

        $invitees_details = array();

        foreach($invitees as $key){
            /* if delete flag is 1 then update record else insert */    
            if($key['deleteFlag'] == 1) {                
                $update_invite_data = array(
                    "deleteFlag"=> 1
                );

                $this->db->where('fromuserId',$userId);
                $this->db->where('searchId',$searchId);
                $this->db->where('inviteEmail',$key['inviteEmail']);
                $is_update = $this->db->update('tribe',$update_invite_data);
                if($is_update){
                    $is_update = true;
                }else {
                    $is_update = false;
                }
            }//if
        }//foreach delete

        /*loop for add new member in tribe*/
        foreach ($invitees as $key) {
            if($key['deleteFlag'] == 0) {
                $email = $key['inviteEmail'];
                $sql = "SELECT * FROM tribe WHERE inviteEmail IN ('$email')  AND fromuserId = $userId AND searchId = $searchId";
                $data = $this->db->query($sql);
                //print_r($data->result_array());exit();
                if($data->num_rows()>0){
                    //update previous record
                    $update_invite_data = array(
                        "deleteFlag"=> 0
                    );
                    
                    $this->db->where('fromuserId',$userId);
                    $this->db->where('searchId',$searchId);
                    $this->db->where('inviteEmail',$key['inviteEmail']);
                    $is_update = $this->db->update('tribe',$update_invite_data);
                    if($is_update){
                        $is_update = true;
                    }else {
                        $is_update = false;
                    }
                   
                }else{
                    $invite_data1= array(
                                    "searchId"=>$searchId,
                                    "fromuserId"=>$userId,
                                    //"name"=> $key['name'],
                                    "deleteFlag"=> 0,
                                    "inviteEmail"=> $key['inviteEmail']
                                );
                    $invitees_details[] =  $invite_data1;
                }
            }
        }//foreach

        if(sizeof($invitees_details)>0){
            $is_update = $this->db->insert_batch('tribe', $invitees_details); 
            if($is_update){
                $_POST['message'] = "Users added in tribe successfully !";
                return $is_update; 
            }
        }else{
            if($is_update){
                $is_update = true;
            } else {
                $is_update = false;
            }
        }
        return $is_update;
       
   }

   /* Function for get the device token of exists users for push notification after invite in tribe*/
   public function get_exists_users($post_data){
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        //print_r($post_data);exit();
        $invitees = array_key_exists("invitees",$post_data);
        if($invitees){ 
            $invitees =  $post_data['invitees']; 
            $emails = array();
            for($i=0;$i<sizeof($invitees);$i++){
                $emails[$i]['inviteEmail'] = $invitees[$i]['inviteEmail'];
            }
          // print_r($emails);exit();
            $str_email = implode(',', array_map(function($el){ return $el['inviteEmail']; }, $emails));
            $str = "'".$str_email."'";
            $email =  str_replace(",","','",$str);
            $userId = $post_data['userId'];
           // echo $email;exit();

           /* $sql = "SELECT userId,firstName,deviceToken,email FROM users 
                    WHERE email IN ($email)";
                    */
            $sql = "SELECT u.userId,u.firstName,u.deviceToken,u.email,t.deleteFlag   
                    FROM users u 
                    LEFT JOIN tribe t ON t.inviteEmail = u.email  
                    WHERE email IN ($email)"; 
            $record = $this->db->query($sql);
            if($record->num_rows()>0){
                //print_r($record->result_array());exit();
                $users =  $record->result_array();
                $ids = array();
                foreach ($users as $key) {
                    $users_id =  array('touserId' =>$key['userId']);
                    $email = $key['email'];
                   // print_r($users_id);exit();
                    $this->db->where('fromuserId',$userId);
                    $this->db->where('searchId',$searchId);
                    $this->db->where('inviteEmail',$email);
                    $this->db->update('tribe',$users_id);

                    // $query = $this->db->get_where('tribe', array('touserId' => $key['userId']));
                    // $row = $query->row();
                    // $name = $row->name;
                    // $firstName = array('firstName' =>$name);
                    // $this->db->where('userId',$key['userId']);
                    // $this->db->update('users',$firstName);

                }

                
                return $record->result_array();
                           // print_r($record->result_array());exit();
            }
        }
   }

   /* Function for get email of non exists users for send email after invite in tribe*/
   public function get_not_exist_emails($post_data){
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        //print_r($post_data);exit();
        $invitees = array_key_exists("invitees",$post_data);
        if($invitees){ 
            $invitees =  $post_data['invitees']; 
            $emails = array();
            $nonExistingEmails = array();
            for($i=0;$i<sizeof($invitees);$i++){
                $email = $invitees[$i]['inviteEmail'];
                $query = $this->db->query("select email from users where email =  '$email'");
                if($query->num_rows()==0){
                    $emails[$i]['inviteEmail'] = $invitees[$i]['inviteEmail']; 
                    $nonExistingEmails[] = $emails[$i]['inviteEmail'];
                }

            }
            return $nonExistingEmails;
        }
   }

   /*function for fetch user information*/
    public function get_user_info($userId,$searchId){
        $sql = "SELECT u.firstName,s.searchName from users u,searchcriteria s where u.userId = s.userId AND  u.userId =$userId AND s.searchId =$searchId AND s.deleteFlag !=1 AND u.deleteFlag !=1";
        $result = $this->db->query($sql);
        return $result->result_array(); 
    }


    /* Count the my tribe members list */
    public function count_get_my_tribe_member_list($post_data){
        $searchId = $post_data['searchId'];
        $loginUserId = $post_data['loginUserId']; 
        
        $sql = "SELECT * FROM tribe WHERE searchId= $searchId AND fromuserId= $loginUserId  AND deleteFlag !=1 AND status = 'accept'";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->num_rows();
        }       
    }

    /* Count the my tribe members list */
    public function get_my_tribe_member_list($post_data){
        $searchId = $post_data['searchId'];
        $loginUserId = $post_data['loginUserId']; 
        
        $sql = "SELECT * FROM tribe WHERE searchId= $searchId AND fromuserId= $loginUserId  AND deleteFlag !=1 AND status = 'accept'";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }       
    }


    /* function for acept or decline invitation */
    public function accept_decline_invitation($post_data){        
        $userId =  $post_data['userId'];
        $searchId =  $post_data['searchId'];
        $status =  $post_data['status'];

        $data  = array('touserId'=>$userId,
                        'searchId'=>$searchId,                       
                       'status' =>$status
                       );
        if($status == 'accept')
        {            
            $sql = "SELECT * FROM tribe WHERE touserId = $userId AND searchId = $searchId AND (status = 'decline' OR status = '')";
            $record = $this->db->query($sql);           
            if($record->num_rows() > 0)
            {  
                $this->db->where('touserId',$userId);
                $this->db->where('searchId',$searchId);
                $this->db->update('tribe',$data);
                if($this->db->affected_rows()>0){
                    $_POST['message'] = 'Invitation accept successfully';
                    return true;
                }else{
                    return false;
                }
            }else{
                    return false;
                }            
        }
        if($status == 'decline')
        {  
            $sql = "SELECT * FROM tribe WHERE touserId = $userId AND searchId = $searchId AND (status = 'accept' OR status = '')";
            $record = $this->db->query($sql);           
            if($record->num_rows() > 0)
            {  
                $this->db->where('touserId',$userId);
                $this->db->where('searchId',$searchId);
                $this->db->update('tribe',$data);
                if($this->db->affected_rows()>0){
                    $_POST['message'] = 'Invitation decline successfully';
                    return true;
                }else{
                    return false;
                }
            }else{
                    return false;
                }            
        }
    }

    /* function for get user and search information */
    public function get_user_serach_info($searchId){
        $sql = "SELECT s.searchName,u.firstName,u.deviceToken FROM searchcriteria s,users u WHERE s.userId = u.userId AND s.searchId = $searchId";
        $record = $this->db->query($sql);           
        if($record->num_rows() > 0)
        {
             return $record->result_array();
        }
    }

    /*function for fetch user information*/
    public function login_user_info($userId){
        $sql = "SELECT firstName,deviceToken FROM users WHERE userId = $userId";
        $result = $this->db->query($sql);
        return $result->result_array(); 
    }

    /* function for shortlist the property */
    public function shortlist_property($post_data){
        unset($post_data['token']);
        //print_r($post_data);exit();
        $propertyUrl = array_key_exists("propertyUrl",$post_data);
        if($propertyUrl){
            unset($post_data['propertyUrl']);
            $post_data['propertyUrl'] = $_POST['propertyUrl1'];   
        }else{
            $post_data['propertyUrl'] = "";
        }  

        $rateCriteria1 = $post_data['rateCriteria1'];
        $rateCriteria1  = empty($rateCriteria1) ? NULL : $rateCriteria1;
       
        $rateCriteria2 = $post_data['rateCriteria2'];
        $rateCriteria2  = empty($rateCriteria2) ? NULL : $rateCriteria2;

        $rateCriteria3 = $post_data['rateCriteria3'];
        $rateCriteria3  = empty($rateCriteria3) ? NULL : $rateCriteria3;

        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        $propertyId = $post_data['propertyId'];
        $propertyName = $post_data['propertyName'];
        $propertyUrl = $post_data['propertyUrl'];
        $commuteTime = $post_data['commuteTime'];
        $description = $post_data['description'];
        $price = $post_data['price'];
        $availableDate = $post_data['availableDate'];

        $shortlistedId = array_key_exists("shortlistedId",$post_data);
        if($shortlistedId){ 
            $shortlistedId =  $post_data['shortlistedId']; 
            unset($post_data['shortlistedId']);
        }

        //echo $shortlistedId;exit();

        $shortlist_details = array('searchId'=> $searchId,
                                   'userId'=> $userId ,
                                   'propertyId'=> $propertyId,
                                   'propertyName'=> $propertyName,
                                   'propertyUrl'=> $propertyUrl ,
                                   'commuteTime'=> $commuteTime,
                                   'description'=> $description,
                                   'price'=> $price,
                                   'availableDate'=> $availableDate
                                );

        if($shortlistedId){
            $shortlistedId = $shortlistedId;
        }else{
            //insert data in shortlistedproperty table
            $inserted = $this->db->insert('shortlistedproperty', $shortlist_details);
            if($inserted){
                $shortlistedId = $this->db->insert_id();
            }else{
                return false;
            }
        }

        
        if($shortlistedId){
            /* Check the rate available or not */
            if($rateCriteria1 || $rateCriteria2 || $rateCriteria3){
                $total_no_rates = 0;
                if($rateCriteria1){
                    $total_no_rates++;
                }
                if($rateCriteria2){
                    $total_no_rates++;
                }
                if($rateCriteria3){
                    $total_no_rates++;
                }

                $average = ($rateCriteria1 + $rateCriteria2 + $rateCriteria3)/$total_no_rates;
                //echo $rateCriteria1;exit();
                $rate_details = array('userId'=> $userId,
                                      'shortlistedId'=> $shortlistedId,
                                      'searchId'=> $searchId,
                                      'propertyName'=> $propertyName,
                                      'rateCriteria1'=> $rateCriteria1,
                                      'rateCriteria2'=> $rateCriteria2 ,
                                      'rateCriteria3'=> $rateCriteria3,
                                      'average' =>$average
                                );
                //print_r($rate_details);exit();
            }else{
                $rate_details = array();
            }

            if(sizeof($rate_details) > 0){
                /* Check user already rate for the shortlisted property */
               $sql = "SELECT * FROM rating WHERE userId =$userId AND shortlistedId = $shortlistedId AND searchId = $searchId";
               $check_property_rated = $this->db->query($sql);
               if($check_property_rated->num_rows()>0){
                    $this->db->where('shortlistedId', $shortlistedId);
                    $this->db->where('searchId', $searchId);
                    $this->db->where('userId', $userId);
                    $save = $this->db->update('rating',$rate_details);
                    $msg = "rate updated";
               }else{
                    $save = $this->db->insert('rating', $rate_details);
                    $msg = "Shortlisted and rated";
               }

               
               /* update average in shortlisted table */
               if($save){
                    $average_update = "SELECT AVG(r.average)as property_avg,AVG(r.rateCriteria1) as avg_rate_criteria1,AVG(r.rateCriteria2) as avg_rate_criteria2,AVG(r.rateCriteria3) as avg_rate_criteria3

                        FROM rating r WHERE r.shortlistedId = $shortlistedId and r.searchId = $searchId AND r.average IS NOT NULL";
                        //echo $average_update;exit();
                    $shortlisted_avg = $this->db->query($average_update);
                    if ($shortlisted_avg->num_rows()>0)
                    {
                       $shortlisted_data = $shortlisted_avg->result_array();
                       $avg = $shortlisted_data[0]['property_avg'];
                       $avgRateCriteria1 = $shortlisted_data[0]['avg_rate_criteria1'];
                       $avgRateCriteria2 = $shortlisted_data[0]['avg_rate_criteria2'];
                       $avgRateCriteria3 = $shortlisted_data[0]['avg_rate_criteria3'];
                       
                       $shortlist_update  = array(
                                                  'avgPropertyRating' =>$avg,
                                                  'avgRateCriteria1' =>$avgRateCriteria1,
                                                  'avgRateCriteria2' =>$avgRateCriteria2,
                                                  'avgRateCriteria3' =>$avgRateCriteria3
                                                );

                       $this->db->where('shortlistedId', $shortlistedId);
                       $this->db->where('searchId', $searchId);
                        if($this->db->update('shortlistedproperty',$shortlist_update))
                        {
                            $_POST['message'] = "Property ".$msg." successfully";
                            return true;
                        }
                    }//shortlisted_avg
               }//insert
            }else{
                $_POST['message'] = "Property shortlisted successfully";
                return true;
            }
        }else{
                return false;
            }
    }

    /* count Shortlisted property list */
    public function count_shortlisted_property_list($post_data){
        $loginUserId = $post_data['loginUserId'];  
        $userId = $post_data['userId'];   
        $searchId = $post_data['searchId']; 
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
        $sortBy = $post_data['sortBy']; //rate/commuteTime
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchId LIKE '%$keyword%' OR s.propertyId LIKE '%$keyword%' OR s.propertyName LIKE '%$keyword%' OR s.commuteTime LIKE '%$keyword%' OR s.price LIKE '%$keyword%' OR s.availableDate LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if($sortBy == "rating"){            
            $sortBy = "ORDER BY s.avgPropertyRating DESC";
        }else if($sortBy == "commuteTime"){
            $sortBy = "ORDER BY s.commuteTime ASC";
        }else{
            $sortBy = "ORDER BY  s.createdAt DESC";
        }


        if (!empty($latitude) && !empty($longitude)) {

            $sql ="SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) FROM comments c where c.shortlistedId = s.shortlistedId AND c.searchId = s.searchId AND c.deleteFlag !=1) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = s.shortlistedId AND r.userId = $loginUserId)) as new_comment_count
                FROM shortlistedproperty s   
                left join users u on s.userId = u.userId
                where s.searchId= $searchId AND s.userId = $userId AND s.deleteFlag != 1
                $keyword $sortBy"; 
                    
        }else{
             $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) FROM comments c where c.shortlistedId = s.shortlistedId AND c.searchId = s.searchId AND c.deleteFlag !=1) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = s.shortlistedId AND r.userId = $loginUserId)) as new_comment_count
                FROM shortlistedproperty s   
                left join users u on s.userId = u.userId
                where s.searchId= $searchId AND s.userId = $userId AND s.deleteFlag != 1
                $keyword $sortBy";
                    
        }

        $record = $this->db->query($sql);
        return $record->num_rows();
    }

    /* Shortlisted property list */
    public function shortlisted_property_list($post_data){
        $loginUserId = $post_data['loginUserId'];
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];
        $sortBy = $post_data['sortBy']; //rate/commuteTime
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR s.idealMovingDate LIKE '%$keyword%' OR s.houseType LIKE '%$keyword%' OR s.priceRange LIKE '%$keyword%' OR s.bedroom LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if($sortBy == "rating"){            
            $sortBy = "ORDER BY s.avgPropertyRating DESC";
        }else if($sortBy == "commuteTime"){
            $sortBy = "ORDER BY s.commuteTime ASC";
        }else{
            $sortBy = "ORDER BY  s.createdAt DESC";
        }

        if (!empty($latitude) && !empty($longitude)) {

            $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) FROM comments c where c.shortlistedId = s.shortlistedId AND c.searchId = s.searchId AND c.deleteFlag !=1) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = s.shortlistedId AND r.userId = $loginUserId)) as new_comment_count
                FROM shortlistedproperty s   
                left join users u on s.userId = u.userId
                where s.searchId= $searchId AND s.userId = $userId AND s.deleteFlag != 1
                $keyword $sortBy LIMIT $offset, $limit";
        }else{

            $sql = "SELECT s.*,u.userId,u.firstName,u.lastName,((SELECT COUNT(*) FROM comments c where c.shortlistedId = s.shortlistedId AND c.searchId = s.searchId AND c.deleteFlag !=1) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = s.shortlistedId AND r.userId = $loginUserId)) as new_comment_count
                FROM shortlistedproperty s   
                left join users u on s.userId = u.userId
                where s.searchId= $searchId AND s.userId = $userId AND s.deleteFlag != 1
                $keyword $sortBy LIMIT $offset, $limit";
        }

        //echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }        
    }

    /* Delete shortlisted property */
    public function delete_shortlisted_property($post_data){
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $data = array('deleteFlag' => 1);

        $this->db->where('userId',$userId);
        $this->db->where('searchId',$searchId);   
        $this->db->where('shortlistedId',$shortlistedId);
        $this->db->update('shortlistedproperty',$data); 
        if($this->db->affected_rows()>0){            
             return true;
        }else{
             return false;
           }
    }

    /*Add comments in shortlisted property*/
    public function add_comments($post_data){       
        unset($post_data['token']);
       // print_r($post_data);exit();
        $shortlistedId = $post_data['shortlistedId'] ;
        $userId = $post_data['userId'] ;
        $searchId = $post_data['searchId'] ;
        $comment =  $post_data['comment'];
        $commentFile = array_key_exists("comment",$post_data);        
        if($commentFile){  
            unset($post_data['comment']);          
            $data = array();
            for($i=0;$i<sizeof($comment);$i++)
            {
                $post_data['shortlistedId'] = $shortlistedId;
                $post_data['userId'] = $userId;
                $post_data['searchId'] = $searchId;
                $post_data['roomType'] = $comment[$i]['roomType'];
                $post_data['commentText'] = $comment[$i]['commentText'];
                $post_data['fileType'] = $comment[$i]['fileType'];
                $post_data['commentFile'] = $_POST['commentFile1'.$i];
                
                 $data[] = $post_data;
            }           
        }

        $is_insert = $this->db->insert_batch('comments',$data);
        if($is_insert){
            return true;
        } else {
            return false;
        }
    }

    /* Get login user info and shortlisted property info for push */
    public function get_user_property_info($post_data){
        $userId = $post_data['userId'];
        $shortlistedId = $post_data['shortlistedId'];

        $sql = "SELECT * FROM users u ,shortlistedproperty s WHERE u.userId = $userId AND s.shortlistedId = $shortlistedId AND s.deleteFlag !=1";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        } 
    }

    /* Get device token of my tribe members for send push notification */
     public function get_tribes_deviceToken($userId,$searchId){ 
        $sql = "SELECT * FROM tribe t  LEFT JOIN users u on t.fromuserId = u.userId OR t.touserId = u.userId WHERE t.searchId = $searchId AND t.status = 'accept' AND t.deleteFlag != 1 AND u.userId != $userId AND u.deviceToken != ''  GROUP BY u.userId";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        } 

    }


    /* for upload video in comment */
    public function uploadData($data){
        return $this->db->insert('comments',$data);
    }

    /* Get shortlisted property detail */
    public function shortlisted_property_detail($post_data){
        $userId = $post_data['userId'];
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];

        $sql = "SELECT s.* FROM shortlistedproperty s where  s.searchId= $searchId AND s.userId = $userId AND s.deleteFlag != 1 AND s.shortlistedId = $shortlistedId";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array()[0];
        } 
    }

    /* Get the shortlisted property comment files */
    public function shortlisted_property_comments_file($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];

        $sql = "SELECT c.commentFile,c.fileType FROM comments c WHERE c.searchId = $searchId AND c.shortlistedId = $shortlistedId AND c.deleteFlag !=1 AND c.commentFile !='' AND c.fileType !=''";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }
    }

    /* Get the comments of roomtype with unread comments count */
    public function comments_roomType_list($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $loginUserId = $post_data['loginUserId'];


        $sql = "SELECT c.searchId,c.shortlistedId,c.roomType,
                ((SELECT COUNT(*) FROM comments c1 where c1.shortlistedId = c.shortlistedId AND c1.searchId = c.searchId AND c1.deleteFlag !=1 AND c1.roomType = c.roomType) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = c.shortlistedId AND r.userId = $loginUserId AND r.roomType = c.roomType)) as unread_comment_count
                FROM comments c WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId GROUP BY c.roomType";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }
    }

    /* Count Get the comments of shortlisted property with perticular roomType*/
    public function count_shortlisted_property_comments($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $roomType = $post_data['roomType'];
        $roomType = "'".$roomType."'";
        $loginUserId = $post_data['loginUserId'];        

       // $sql = "SELECT c.* FROM comments c WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType ='".$roomType."' AND c.deleteFlag !=1";
        $sql = "SELECT c.*,cr.userId as readUserId,cr.commentId as readCommentId,cr.shortlistedId as readShortlistedId,cr.roomType as readRoomType,u.firstName,u.lastName FROM comments c 
            LEFT JOIN commentsread cr ON cr.shortlistedId = c.shortlistedId AND cr.userId = $loginUserId AND c.commentId = cr.commentId
            LEFT JOIN users u ON u.userId = c.userId

            WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType =$roomType AND c.deleteFlag !=1 ORDER BY c.createdAt DESC";
       // echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->num_rows();
        }
    }

    /* Insert  the comments of shortlisted property with perticular roomType into commentRead table*/
    public function shortlisted_property_comments_read($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $roomType = $post_data['roomType'];
        $roomType = "'".$roomType."'";
        $loginUserId = $post_data['loginUserId'];

       // $sql = "SELECT c.* FROM comments c WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType ='".$roomType."' AND c.deleteFlag !=1";
        $sql = "SELECT c.*,cr.userId as readUserId,cr.commentId as readCommentId,cr.shortlistedId as readShortlistedId,cr.roomType as readRoomType,u.firstName,u.lastName FROM comments c 
            LEFT JOIN commentsread cr ON cr.shortlistedId = c.shortlistedId AND cr.userId = $loginUserId AND c.commentId = cr.commentId
            LEFT JOIN users u ON u.userId = c.userId

            WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType = $roomType AND c.deleteFlag !=1";
       // echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            $data = $record->result_array();

            $insert_data = array();
            foreach ($data as $detail) {
                //print_r($detail);exit();
               if(!$detail['readUserId']){
                  $readdata = array(
                                    'userId' => $loginUserId,
                                    'commentId' => $detail['commentId'] ,
                                    'shortlistedId' => $detail['shortlistedId'],
                                    'roomType' => $detail['roomType'],
                                    'readFlag' => 1
                                );
                  $insert_data[] = $readdata;
               }
            }//foreach

            if(count($insert_data)>0){
               return $this->db->insert_batch('commentsread', $insert_data); 
            }else{
                return true;
            }
        }else{
                return false;
            }
    }

    /* Get the comments of shortlisted property with perticular roomType*/
    public function shortlisted_property_comments($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $roomType = $post_data['roomType'];
        $roomType = "'".$roomType."'";
        $loginUserId = $post_data['loginUserId'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];

       // $sql = "SELECT c.* FROM comments c WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType ='".$roomType."' AND c.deleteFlag !=1";
        $sql = "SELECT c.*,cr.userId as readUserId,cr.commentId as readCommentId,cr.shortlistedId as readShortlistedId,cr.roomType as readRoomType,u.firstName,u.lastName FROM comments c 
            LEFT JOIN commentsread cr ON cr.shortlistedId = c.shortlistedId AND cr.userId = $loginUserId AND c.commentId = cr.commentId
            LEFT JOIN users u ON u.userId = c.userId

            WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType = $roomType AND c.deleteFlag !=1 ORDER BY c.createdAt DESC LIMIT $offset,$limit";
       // echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return$record->result_array();
        }
    }

    /* Get the unread comment count*/
    public function get_unread_comment_count($post_data){
        $searchId = $post_data['searchId'];
        $shortlistedId = $post_data['shortlistedId'];
        $loginUserId = $post_data['loginUserId'];
        $roomType = $post_data['roomType'];
        $roomType = "'".$roomType."'";
        $sql = "SELECT 
                ((SELECT COUNT(*) FROM comments c1 where c1.shortlistedId = c.shortlistedId AND c1.searchId = c.searchId AND c1.deleteFlag !=1 AND c1.roomType = c.roomType) - (SELECT COUNT(*) FROM commentsread r where r.shortlistedId = c.shortlistedId AND r.userId = $loginUserId AND r.roomType = c.roomType)) as unread_comment_count
                FROM comments c WHERE c.shortlistedId = $shortlistedId AND c.searchId =$searchId AND c.roomType = $roomType GROUP BY c.roomType";
               // echo $sql;exit();
        $record = $this->db->query($sql);        
            return $record->row('unread_comment_count');
    }


    /* Insert chat message */
    public function add_chat($post_data){
        $searchId = $post_data['searchId'];
        $loginUserId = $post_data['loginUserId'];
        $chatText = $post_data['chatText'];
        $data  = array(
                        'userId'=> $loginUserId,
                        'searchId'=>$searchId,
                        'deleteFlag'=>0,
                        'chatText'=>$chatText
                    );
        return $this->db->insert('chat',$data);
    }

    /* Fetch the perticular serch's chat messages Count*/
    public function count_search_detail_chats($post_data){
        $searchId = $post_data['searchId'];

        $sql = "SELECT c.*,u.firstName,u.lastName FROM chat c,users u 
                WHERE  c.searchId = $searchId  AND c.deleteFlag !=1 AND c.userId = u.userId
                ORDER BY c.createdAt DESC";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->num_rows();
        }else{
            return false;
        }
    }

    /* Fetch the perticular serch's chat messages*/
    public function search_detail_chats($post_data){
        $searchId = $post_data['searchId'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];

        $sql = "SELECT c.*,u.firstName,u.lastName FROM chat c,users u 
                WHERE  c.searchId = $searchId  AND c.deleteFlag !=1 AND c.userId = u.userId
                ORDER BY c.createdAt DESC
                LIMIT $offset,$limit";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }else{
            return false;
        }
    }

   /* count my search criteria and tribe search with latest chat list */
   public function count_search_list_latest_chats($post_data){
        $userId = $post_data['loginUserId'];  
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR s.idealMovingDate LIKE '%$keyword%' OR s.houseType LIKE '%$keyword%' OR s.priceRange LIKE '%$keyword%' OR s.bedroom LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if (!empty($latitude) && !empty($longitude)) {

            $sql = "SELECT s.*,c.chatId,c.userId as chatUserId,u.firstName,u.lastName,c.searchId as chatSearchId,c.chatText,c.deleteFlag,c.createdAt as chatDateTime
                 FROM searchcriteria s  
                 LEFT JOIN tribe t ON s.searchId = t.searchId AND t.fromuserId = $userId
                 LEFT JOIN shortlistedproperty p ON s.searchId = p.searchId 
                 LEFT JOIN chat c ON  s.searchId = c.searchId  AND c.createdAt = (SELECT MAX(createdAt) FROM chat WHERE chat.searchId = s.searchId) AND c.searchId IS NOT NULL AND c.deleteFlag !=1
                 LEFT JOIN users u ON c.userId = u.userId 
                 WHERE s.searchId IN (SELECT t.searchId FROM tribe t WHERE (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId 
                 $keyword GROUP BY s.searchId ORDER BY s.userId = $userId DESC";
                    
        }else{
             $sql = "SELECT s.*,c.chatId,c.userId as chatUserId,u.firstName,u.lastName,c.searchId as chatSearchId,c.chatText,c.deleteFlag,c.createdAt as chatDateTime
                 FROM searchcriteria s  
                 LEFT JOIN tribe t ON s.searchId = t.searchId AND t.fromuserId = $userId
                 LEFT JOIN shortlistedproperty p ON s.searchId = p.searchId 
                 LEFT JOIN chat c ON  s.searchId = c.searchId  AND c.createdAt = (SELECT MAX(createdAt) FROM chat WHERE chat.searchId = s.searchId) AND c.searchId IS NOT NULL AND c.deleteFlag !=1
                 LEFT JOIN users u ON c.userId = u.userId 
                 WHERE s.searchId IN (SELECT t.searchId FROM tribe t WHERE (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId 
                 $keyword GROUP BY s.searchId ORDER BY s.userId = $userId DESC";
                    
        }

        $record = $this->db->query($sql);
        return $record->num_rows();
   }

   /* count my search criteria and tribe search with latest chat list */
   public function search_list_latest_chats($post_data){
        $userId = $post_data['loginUserId'];
        $keyword = $post_data['keyword'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];
       // $filter = $post_data['filter']; //fav_teacher/all
       // $type =  $post_data['type']; // upcoming/past
        

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR s.idealMovingDate LIKE '%$keyword%' OR s.houseType LIKE '%$keyword%' OR s.priceRange LIKE '%$keyword%' OR s.bedroom LIKE '%$keyword%')";
        }

        if(!empty($latitude) && !empty($longitude))
        {
            $distance_in_km = "( 6371 * acos( cos( radians($latitude) ) * cos( radians( lc.latitude) ) 
                * cos( radians( lc.longitude ) - radians($longitude) ) + 
                    sin( radians($latitude) ) * sin( radians( lc.latitude ) ) ) ) 
                AS distance_in_km";
        }

        if (!empty($latitude) && !empty($longitude)) {
                $sql = "SELECT s.*,c.chatId,c.userId as chatUserId,u.firstName,u.lastName,c.searchId as chatSearchId,c.chatText,c.deleteFlag,c.createdAt as chatDateTime
                     FROM searchcriteria s  
                     LEFT JOIN tribe t ON s.searchId = t.searchId AND t.fromuserId = $userId
                     LEFT JOIN shortlistedproperty p ON s.searchId = p.searchId 
                     LEFT JOIN chat c ON  s.searchId = c.searchId  AND c.createdAt = (SELECT MAX(createdAt) FROM chat WHERE chat.searchId = s.searchId) AND c.searchId IS NOT NULL AND c.deleteFlag !=1
                     LEFT JOIN users u ON c.userId = u.userId 
                     WHERE s.searchId IN (SELECT t.searchId FROM tribe t WHERE (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId 
                     $keyword GROUP BY s.searchId ORDER BY s.userId = $userId DESC LIMIT $offset, $limit";
        }else{

                $sql = "SELECT s.*,c.chatId,c.userId as chatUserId,u.firstName,u.lastName,c.searchId as chatSearchId,c.chatText,c.deleteFlag,c.createdAt as chatDateTime
                     FROM searchcriteria s  
                     LEFT JOIN tribe t ON s.searchId = t.searchId AND t.fromuserId = $userId
                     LEFT JOIN shortlistedproperty p ON s.searchId = p.searchId 
                     LEFT JOIN chat c ON  s.searchId = c.searchId  AND c.createdAt = (SELECT MAX(createdAt) FROM chat WHERE chat.searchId = s.searchId) AND c.searchId IS NOT NULL AND c.deleteFlag !=1
                     LEFT JOIN users u ON c.userId = u.userId 
                     WHERE s.searchId IN (SELECT t.searchId FROM tribe t WHERE (t.touserId =$userId OR t.fromuserId =$userId) AND t.status = 'accept') OR s.userId =$userId 
                     $keyword GROUP BY s.searchId ORDER BY s.userId = $userId DESC LIMIT $offset, $limit";
        }
        //echo $sql;exit();
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }        
   }

   /* Connect to my sherpa */
   public function connect_to_my_sherpa($post_data){
        $userId = $post_data['loginUserId'];  
        $data = array('connectSherpaFlag'=> 1);
        $this->db->where('userId',$userId);
        $record = $this->db->update('users',$data);
        if($record){
            return true;
        }else{
            return false;
        }
   }

   /*function for fetch user and search information*/
    public function get_user_search_info($userId,$searchId){
        $sql = "SELECT u.firstName,s.searchName from users u,searchcriteria s where u.userId =$userId AND s.searchId =$searchId AND s.deleteFlag !=1 AND u.deleteFlag !=1 ";
        $result = $this->db->query($sql);
        return $result->result_array(); 
    }

    /* Get count of my invitation list */
    public function count_MyInvitationList($post_data){
        $userId = $post_data['userId'];
        $keyword = $post_data['keyword'];

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR u.firstName LIKE '%$keyword%' OR s.searchId LIKE '%$keyword%' OR u.userId LIKE '%$keyword%' OR t.inviteEmail LIKE '%$keyword%' OR u.email LIKE '%$keyword%')";
        }

        $sql = "SELECT t.*,u.userId,u.firstName,u.email,s.searchName FROM tribe t
                LEFT JOIN users u ON t.fromuserId = u.userId AND u.deleteFlag !=1
                LEFT JOIN searchcriteria s on t.searchId = s.searchId 
                WHERE  t.touserId = $userId $keyword AND t.deleteFlag !=1 AND  s.deleteFlag !=1 ORDER BY t.createdAt DESC";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->num_rows();
        }else{
            return false;
        }
    }

    /* Get my invitation list */    
    public function MyInvitationList($post_data){
        $userId = $post_data['userId'];
        $keyword = $post_data['keyword'];
        $limit = $post_data['limit'];
        $offset = $post_data['offset'];

        if(!empty($keyword)){
            $keyword = "AND (s.searchName LIKE '%$keyword%' OR u.firstName LIKE '%$keyword%' OR s.searchId LIKE '%$keyword%' OR u.userId LIKE '%$keyword%' OR t.inviteEmail LIKE '%$keyword%' OR u.email LIKE '%$keyword%')";
        }

        $sql = "SELECT t.*,u.userId,u.firstName,u.email,s.searchName FROM tribe t
                LEFT JOIN users u ON t.fromuserId = u.userId AND u.deleteFlag !=1
                LEFT JOIN searchcriteria s on t.searchId = s.searchId 
                WHERE  t.touserId = $userId $keyword AND t.deleteFlag !=1 AND  s.deleteFlag !=1 ORDER BY t.createdAt DESC LIMIT $offset,$limit";
        $record = $this->db->query($sql);
        if($record->num_rows()>0){
            return $record->result_array();
        }else{
            return false;
        }
    }


}?>