<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Upload extends CI_Controller {

	public function __construct()
	{	
            parent::__construct();
           
            include APPPATH . 'libraries/classes/class.phpmailer.php';
            $this->config->load('my_constants');
	    $this->load->model('mobile_api/Common_model');
            $this->load->model('mobile_api/Auth_model');
            $this->load->helper('security');
            $this->load->helper('string');
            $this->load->helper('form');
	}
        public function test_video_upload(){
            
            $this->load->view('admin/test_upload_video');	
        }
        
        public function insert_video() {
            $configVideo['upload_path'] = 'uploads/comment/video';
            $configVideo['allowed_types'] = 'wmv|mp4|avi|mov';
            $configVideo['max_size'] = '1000000';
            $configVideo['overwrite'] = FALSE;
            $configVideo['remove_spaces'] = TRUE;
            $path = $_FILES['commentFile']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $video_name = random_string('numeric', 5);
            $configVideo['file_name'] = $video_name.".".$ext;
            $this->load->library('upload', $configVideo);
            $this->upload->initialize($configVideo);
            if (!$this->upload->do_upload('commentFile')) {
                $response = array(
                    'ResponseCode' => 0,
                    'ResponseMessage' => 'FAILURE', 
                    'Comments' => strip_tags($this->upload->display_errors()),
                    'Result' =>false,
                    'Status' =>400
                 );
                 echo json_encode($response);
            } else {
                if($_FILES['commentFile']['type'] == 'video/mp4' || $_FILES['commentFile']['type'] == 'video/wmv' || $_FILES['commentFile']['type'] == 'video/avi' || $_FILES['commentFile']['type'] == 'video/mov'){

                    
                    $url = 'uploads/comment/video/'.$configVideo['file_name'];
                }
                if($url){
                    $data = array(
                        'commentFile' => $url,
                        'commentText' =>  $this->input->post('commentText'),
                        'userId' =>  $this->input->post('userId'),
                        'searchId' =>  $this->input->post('searchId'),
                        'shortlistedId' =>  $this->input->post('shortlistedId'),
                        'roomType' => $this->input->post('roomType'),
                        'fileType' => $this->input->post('fileType')//$_FILES['commentFile']['type']
                    ); 
                    $is_insert =  $this->Common_model->uploadData($data);
                    if($is_insert){
                        $post_data = array();
                        $post_data['userId'] = $this->input->post('userId');
                        $post_data['searchId'] = $this->input->post('searchId');
                        $post_data['shortlistedId'] = $this->input->post('searchId');
                        /* Send notification to exist members */
                        /* get the login user info and shortlisted property info for push */
                        $user_info = $this->Common_model->get_user_property_info($post_data);
                        if($user_info){
                            $user_name = $user_info[0]['firstName'];
                            $propertyName = $user_info[0]['propertyName'];
                            $propertyId = $user_info[0]['propertyId'];

                            /* Send notification to my tribe members */
                            $get_tribes_deviceToken = $this->Common_model->get_tribes_deviceToken($post_data);
                            if($get_tribes_deviceToken){

                                $deviceToken = ARRAY();
                                foreach ($get_tribes_deviceToken as $row) {
                                    $deviceToken[] = $row["deviceToken"];                           
                                }

                                $message = $this->config->item('notification_add_comment_on_shortlisted_property');
                                $message_to_push = str_replace("firstName", $user_name, $message[0]);                       
                                $message_to_push_event = str_replace("propertyName", $propertyName, $message_to_push);
                                $message_to_push_notification = $message_to_push_event;

                                $payload = json_encode([
                                    'aps' => [
                                        'alert' => $message_to_push_notification,
                                        'sound' => 'cat.caf',
                                        'badge' => 1,
                                        'content-available' => 1
                                    ],
                                    "push_type"=> $message[1],
                                    "userId"=> $this->input->post('userId'),
                                    "searchId"=> $this->input->post('searchId'),
                                    "shortlistedId"=> $this->input->post('shortlistedId')
                                ]); 
                                //print_r($payload);exit();
                                //$this->send_multiple_user_notification_ios($deviceToken, $payload);
                            }
                        }

                        $response = array(
                            'ResponseCode' => 1,
                            'ResponseMessage' => 'SUCCESS', 
                            'Comments' => 'Uploaded video successfully',
                            'Result' =>$is_insert,
                            'Status' =>200
                         );

                        
                    } else {
                        unlink($url);
                        $response = array(
                            'ResponseCode' => 0,
                            'ResponseMessage' => 'FAILURE', 
                            'Comments' => 'Could not upload video',
                            'Result' =>$is_insert,
                            'Status' =>400
                         );
                    }
                }else{
                    $response = array(
                            'ResponseCode' => 0,
                            'ResponseMessage' => 'FAILURE', 
                            'Comments' => 'File you are trying to upload is not valid',
                            'Result' =>false,
                            'Status' =>400
                         );
                }

                
                echo json_encode($response);
            }
        }


    /*send notification for multiple users*/
    public function send_multiple_user_notification_ios($deviceToken, $payload)
    {
        //print_r($deviceToken);exit();
        $passphrase = '123456'; // change this to your passphrase(password)

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert','Crowded_Final.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert','Satori_Dev_Final.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        //stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');

        // Open a connection to the APNS server
        // for 
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); 

      /*  $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); */

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        foreach($deviceToken as $token)
        {
            $msg = chr(0) . pack('n',32) . pack('H*', $token) . pack('n',strlen($payload)) . $payload;
            // Build the binary notification
            //$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));
        }

        fclose($fp);
        if (!$result){
            return false;
            //echo '<br>Message not delivered' . PHP_EOL . print_r($result);
        }            
        else{
            return true;
            //echo '<br>Message successfully delivered' . PHP_EOL . print_r($result);
        }
    }
        
    }
?>