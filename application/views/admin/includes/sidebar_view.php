<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li class="<?php if(!empty($active_dashboard)){echo $active_dashboard;}?>">
                            <a href="<?php echo site_url();?>admin/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <?php $role = $this->session->userdata('role');
                              if ($role!=='teacher') {
                                 if ($role!=='student' && $role!=='studio' ) {

                        ?>
                        <li class="<?php if(!empty($active_package)){echo $active_package;}?>">
                            <a href="#"><i class="fa fa-suitcase"></i> Packages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Package/add_package">Add Package</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Package/show_packages/10/nosearch/price/asc/1">Show Packages</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="<?php if(!empty($active_location)){echo $active_location;}?>">
                            <a href="#"><i class="fa fa-location-arrow fa-lg"></i> Locations<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Location/add_location">Add Location</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Location/show_locations/10/nosearch/name1/asc/1">Show Locations</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="<?php if(!empty($active_style)){echo $active_style;}?>">
                            <a href="#"><i class="fa fa-line-chart"></i> Styles<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Style/add_style">Add Style</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Style/show_styles/10/nosearch/name/asc/1">Show Styles</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="<?php if(!empty($active_level)){echo $active_level;}?>">
                            <a href="#"><i class="fa fa-line-chart"></i> Levels<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Level/add_level">Add Level</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Level/show_levels/10/nosearch/name/asc/1">Show Levels</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="<?php if(!empty($active_teacher)){echo $active_teacher;}?>">
                            <a href="#"><i class="fa fa-users"></i> Teachers<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Teacher/add_teacher">Add Teacher</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Teacher/show_teachers/10/nosearch/name/asc/1">Show Teacher</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>  
                          
                        <li class="<?php if(!empty($active_class)){echo $active_class;}?>">
                            <a href="#"><i class="fa fa-users"></i> Classes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Classes/add_class">Add Class</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Classes/show_classes/10/nosearch/date/asc/1">Show Classes</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li class="<?php if(!empty($active_student)){echo $active_student;}?>">
                            <a href="#"><i class="fa fa-graduation-cap"></i> Students<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_students/10/nosearch/firstname/asc/1">Students</a>
                                </li> 
                                <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_classes/10/nosearch/date/asc/1">Student Classes</a>
                                </li>   
                                <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_packages/10/nosearch/date/asc/1">Student Packages</a>
                                </li>  
                                <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_favorite_teachers/10/nosearch/studentname/asc/1">Student Favourite Teacher</a>
                                </li>                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li class="<?php if(!empty($active_studio)){echo $active_studio;}?>">
                            <a href="#"><i class="fa fa-picture-o"></i> Studios <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url();?>admin/Studio/add_studio">Add Studio</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>admin/Studio/show_studios/10/nosearch/name/asc/1">Show Studios</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <?php }}
                            if ($role =='studio') {?>
                            <li class="<?php if(!empty($active_class)){echo $active_class;}?>">
                                <a href="#"><i class="fa fa-users"></i> Studio Classes <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo site_url();?>admin/Classes/add_class">Add Class</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url();?>admin/Classes/show_classes/10/nosearch/date/asc/1">Show Classes</a>
                                    </li>                                
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li class="<?php if(!empty($active_student)){echo $active_student;}?>">
                            <a href="#"><i class="fa fa-graduation-cap"></i> Studio Students<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <!-- <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_students/10/nosearch/firstname/asc/1">Students</a>
                                </li> --> 
                                <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_classes/10/nosearch/date/asc/1">Student Classes</a>
                                </li>   
                               <!--  <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_packages/10/nosearch/date/asc/1">Student Packages</a>
                                </li>   -->
                                <!-- <li>
                                    <a href="<?php echo site_url();?>admin/Student/show_student_favorite_teachers/10/nosearch/studentname/asc/1">Student Favourite Teacher</a>
                                </li>    -->                            
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                         <?php  }?> 
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>