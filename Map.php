<html>
  <head>
  <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
  <script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <style type="text/css">
  body {margin: 0;}
  </style>
  </head>
  <body>
  <div id="map" style="height: 100%;"></div>
  <script type="text/javascript" >

var startingLocation = "Oxford Street London";

$.ajax("http://maps.google.com/maps/api/geocode/json?address=" + startingLocation).done(function(results){
  var location = results.results[0].geometry.location

var request = {
  "departure_searches": [
  {
  "id": "public transport from oxford street",
  "coords": location,
  "transportation": {
  "type": "public_transport"
  },
  "departure_time": new Date().toISOString(),
  "travel_time": 1800
  }
  ]
  };

var headers = {
  "X-Application-Id": "id",
  "X-Api-Key": "key"
  }

$.ajax({
  url : "http://api.traveltimeapp.com/v4/time-map",
  type: "post",
  headers: headers,
  data: JSON.stringify(request),
  contentType: "application/json; charset=UTF-8",
  success: function(data) {

var osmUrl="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
  var osmTileLayer = L.tileLayer(osmUrl, {minZoom: 8, maxZoom: 15});
  var map = L.map("map").addLayer(osmTileLayer).setView(location, 12);

data.results[0].shapes.forEach(function(s) {
  var holes = s.holes;
  holes.unshift(s.shell);
  L.polygon(holes).addTo(map);
  });
  }
  })
  })
  </script>
  </body>
  </html>